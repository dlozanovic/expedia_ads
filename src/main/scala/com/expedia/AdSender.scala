package com.expedia

import org.apache.spark.sql.{DataFrame, Row, SparkSession}

import scala.util.Try

class AdSender extends Serializable {

  def takeElements(it: Iterator[Row], adsPerBatch: Int): List[Row] = it.take(adsPerBatch).toList

  def sendAds(it: Iterator[Row], serviceClient: ServiceClient, adsPerBatch: Int = 25) = {
    while (it.hasNext) {
      val rows = takeElements(it, adsPerBatch)

      val language = rows.head.getString(0)
      val ads = rows.map(r => (r.getLong(1), r.getLong(2)))

      serviceClient.sendAdIds(language, ads)
    }
  }

  def sendAdsToServiceClient(serviceClient: ServiceClient, df: DataFrame)(implicit spark: SparkSession): Try[Unit] = {
    import spark.implicits._
    Try(
      df.select("language", "ad_group_id", "ad_id")
        .repartition($"language")
        .foreachPartition(it => sendAds(it, serviceClient))

    )
  }

}
