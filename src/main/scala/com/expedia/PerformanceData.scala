package com.expedia

import java.time.LocalDate

import org.apache.spark.sql.{DataFrame, SparkSession}

class PerformanceData extends Serializable {

  def readPerformanceData()(implicit spark: SparkSession): DataFrame = {
    spark.sql("SELECT * FROM ad_performance")
  }

  def readThreeMonthsOldPerformanceData(searchEngine: String, now: LocalDate, adType: String = "TEXT")
                                       (implicit spark: SparkSession): DataFrame = {
    val df = readPerformanceData()
    val filterDate = threeMonthsBefore(now)
    filterPerformanceData(df, searchEngine, filterDate, adType)
  }

  def filterPerformanceData(df: DataFrame, searchEngine: String, date: LocalDate, adType: String)
                           (implicit spark: SparkSession): DataFrame = {

    import spark.implicits._
    df.filter($"search_engine" === searchEngine && $"report_date" >= date.toString && $"ad_type" === adType)
  }

  def threeMonthsBefore(date: LocalDate): LocalDate = date.minusMonths(3)


}
