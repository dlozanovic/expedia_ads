package com.expedia

import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions._

class TopPerformingAds extends Serializable {

  def aggregateClicks(performanceData:DataFrame)(implicit spark: SparkSession): DataFrame = {
    //I'm assuming that the data is "clean", and that for each ad_id, ad_group_id combination account_id is the same
    //otherwise I need to add account_id into groupBy
    performanceData.groupBy("ad_group_id","ad_id")
      .agg(first("account_id").as("account_id"),
        sum("clicks").as("clicks"))
  }

  def calculateTopN(df:DataFrame, topN:Int)(implicit spark: SparkSession):DataFrame = {
    import spark.implicits._
    val window = Window.partitionBy("country").orderBy($"clicks".desc)

    // The task 3 problem is not defined for one corner case if we have on 100th position more than one ad
    // I'm assuming that I should take only one of them, if I need to take
    df.withColumn("topN", row_number().over(window))
      .filter($"topN" <= topN)
      .drop("topN")
  }

  def topPerformingAds(performanceData:DataFrame, accounts:DataFrame, topN:Int=100 )(implicit spark: SparkSession): DataFrame = {
    val aggregatedPerformanceData = aggregateClicks(performanceData)
    val joinData = aggregatedPerformanceData.join(accounts,"account_id")
    calculateTopN(joinData, topN)
  }

}
