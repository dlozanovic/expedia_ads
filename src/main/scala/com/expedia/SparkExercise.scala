package com.expedia

import java.time.LocalDate

import org.apache.spark.sql.SparkSession

import scala.util.Try

trait ServiceClient {
  type AdGroupId = Long
  type AdId = Long

  def sendAdIds(language: String, adIdsBatch: Seq[(AdGroupId, AdId)]): Try[Unit]
}

object SparkExercise {

  def main(args: Array[String]): Unit = {
    implicit val spark: SparkSession = SparkSession.builder
      .master("local")
      .appName("spark-exercise-app")
      .enableHiveSupport
      .getOrCreate

    // stub
    val serviceClient = new ServiceClient with Serializable {
      override def sendAdIds(language: String, adIdsBatch: Seq[(AdGroupId, AdId)]): Try[Unit] = Try()
    }

    val searchEngine = "GOOGLE"

    performTasks(
      serviceClient,
      searchEngine,
      new PerformanceData,
      new AccountData,
      new TopPerformingAds,
      new AdSender
    )

  }

  def performTasks(serviceClient: ServiceClient,
                   searchEngine:String,
                   performanceData: PerformanceData,
                   accountData: AccountData,
                   topPerformingAds: TopPerformingAds,
                   adSender: AdSender
                  )(implicit spark: SparkSession):Unit = {

    val performanceDataFrame = performanceData.readThreeMonthsOldPerformanceData(searchEngine, LocalDate.now)

    val accounts = accountData.readAccounts(searchEngine)

    val topPerformingAdsDataFrame = topPerformingAds.topPerformingAds(performanceDataFrame, accounts)

    adSender.sendAdsToServiceClient(serviceClient, topPerformingAdsDataFrame)
  }
}
