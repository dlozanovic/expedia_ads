package com.expedia

import org.apache.spark.sql.{DataFrame, SparkSession}

import scala.util.Try

class AccountData extends Serializable {

  def latestPartitionDate(searchEngine:String)(implicit spark: SparkSession):Option[String] = {
    // possible SQL injection, alternative is to take all partitions and then filter out only that contains searchEngine string
    val df = spark.sql(s"SHOW PARTITIONS accounts partition(search_engine='$searchEngine')")
    val partitions = df.collect()

    Try {
      val latestPartition = partitions.map(_.getString(0)).max

      latestPartition.split("/")
        .filter(_.startsWith("report_date"))
        .head
        .split("=")(1)
    }.toOption
  }

  def readAllAccounts(searchEngine:String, date:String)(implicit spark: SparkSession) = {
    import spark.implicits._

    spark.sql("SELECT * FROM accounts")
      .filter($"search_engine" === searchEngine && $"report_date" === date)
  }

  def filterAccounts(df:DataFrame,languages:Iterable[String], status:String= "ACTIVE")(implicit spark: SparkSession) = {
    import spark.implicits._

    df.filter($"language".isInCollection(languages) && $"account_status" === status )
  }

  def readAccounts(searchEngine:String)(implicit spark: SparkSession) = {
    latestPartitionDate(searchEngine) match {
      case Some(latestDate) =>
        val allAccounts = readAllAccounts(searchEngine,latestDate)
        filterAccounts(allAccounts,Set("ENG","SPA"))
      case None =>
        spark.sql("SELECT * FROM accounts WHERE 1!=1")
    }
  }

}
