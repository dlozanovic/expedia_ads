package com.expedia

import java.io.File
import java.time.{LocalDate, Period}

import com.holdenkarau.spark.testing.DataFrameSuiteBase
import org.apache.spark.SparkConf
import org.apache.spark.sql.internal.StaticSQLConf.CATALOG_IMPLEMENTATION
import org.scalatest.{BeforeAndAfterAll, FlatSpec}

class PerformanceDataTest extends FlatSpec with BeforeAndAfterAll with DataFrameSuiteBase {

  override def conf: SparkConf = super.conf.set(CATALOG_IMPLEMENTATION.key, "hive")


  lazy implicit val _spark = spark

  val testObject = new PerformanceData
  import testObject._

  override def beforeAll(): Unit = {
    super.beforeAll()

    spark.sql(
      """
        |CREATE TABLE `ad_performance` (
        |  `account_id` bigint,
        |  `ad_group_id` bigint     COMMENT 'ad group id is globally unique (also between accounts)',
        |  `ad_id` bigint           COMMENT 'ad id is only unique within an ad group',
        |  `ad_type` string         COMMENT 'TEXT or IMAGE',
        |  `clicks` bigint          COMMENT 'number of clicks an ad received on specific day'
        |  )
        |PARTITIONED BY (
        |  `search_engine` string   COMMENT 'e.g. GOOGLE or BING',
        |  `report_date` string     COMMENT 'format YYYY-MM-dd, e.g. 2020-10-12'
        |)
        |STORED AS PARQUET
        |""".stripMargin)

    spark.sql(
      """INSERT INTO ad_performance PARTITION (search_engine='GOOGLE', report_date='2020-06-12')
        |VALUES (1,1,1,'TEXT',42)
        |""".stripMargin)

    spark.sql(
      """INSERT INTO ad_performance PARTITION (search_engine='GOOGLE', report_date='2020-06-12')
        |VALUES (2,2,2,'IMAGE',28)
        |""".stripMargin)

    spark.sql(
      """INSERT INTO ad_performance PARTITION (search_engine='BING', report_date='2020-06-12')
        |VALUES (3,3,3,'TEXT',2)
        |""".stripMargin)

    spark.sql(
      """INSERT INTO ad_performance PARTITION (search_engine='GOOGLE', report_date='2019-06-12')
        |VALUES (4,4,4,'TEXT',28)
        |""".stripMargin)

  }

  override def afterAll(): Unit = {
    super.afterAll()

    import scala.reflect.io.Directory

    val metastore = new Directory(new File("metastore_db"))
    metastore.deleteRecursively()

    val warehouse = new Directory(new File("spark-warehouse"))
    warehouse.deleteRecursively()

  }

  "threeMonthsBefore" should "give a date 3 months before" in {
    val now = LocalDate.now
    val past = threeMonthsBefore(now)

    assert(Period.between(past, now) == Period.of(0, 3, 0))
  }

  "readPerformanceData" should "give all data from ad_performance " in {
    val df = readPerformanceData()
    assert(df.count() == 4)
  }

  "filterPerformanceData" should "filter out data based od search_engine and report_date" in {
    val df = spark.sql("SELECT * from ad_performance").cache()

    val filter1 = filterPerformanceData(df, "GOOGLE", LocalDate.of(2020, 1, 1), "TEXT")
    assert(filter1.count == 1)

    val filter2 = filterPerformanceData(df, "GOOGLE", LocalDate.of(2019, 1, 1), "TEXT")
    assert(filter2.count == 2)

    val filter3 = filterPerformanceData(df, "BING", LocalDate.of(2019, 1, 1), "TEXT")
    assert(filter3.count == 1)

  }

  "readThreeMonthsOldPerformanceData" should "read the 3 months data" in {
    val now = LocalDate.of(2020, 6, 22)
    val df = readThreeMonthsOldPerformanceData("GOOGLE", now, "TEXT")

    assert(df.count() == 1)
    assert(df.head.getAs[Long]("account_id") == 1)

  }

}
