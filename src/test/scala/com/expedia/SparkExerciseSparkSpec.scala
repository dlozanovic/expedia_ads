package com.expedia

import org.apache.spark.sql.{DataFrame, SparkSession}
import org.mockito.stubbing.Answer
import org.mockito.{ArgumentMatchersSugar, MockitoSugar}
import org.scalatest.FlatSpec

import scala.util.Try


class SparkExerciseSparkSpec extends FlatSpec with MockitoSugar with ArgumentMatchersSugar {

  import SparkExercise._

  "performTasks" should "call all components" in {
    implicit val spark = mock[SparkSession]

    val searchEngine = "GOOGLE"
    val serviceClient = mock[ServiceClient]
    val performanceData = mock[PerformanceData]
    val accountData = mock[AccountData]
    val topPerformingAds = mock[TopPerformingAds]
    val adSender = mock[AdSender]

    val performanceDataFrame = mock[DataFrame]
    val accountDataFrame = mock[DataFrame]
    val topPerformingAdsDataFrame = mock[DataFrame]

    when(performanceData.readThreeMonthsOldPerformanceData(*, *, *)(*)).thenReturn(performanceDataFrame)
    when(accountData.readAccounts(same(searchEngine))(*)).thenReturn(accountDataFrame)
    when(topPerformingAds.topPerformingAds(*,*,*)(*)).thenReturn(topPerformingAdsDataFrame)
    when(adSender.sendAdsToServiceClient(*,*)(*)).thenReturn(Try())

    performTasks(serviceClient, searchEngine, performanceData, accountData, topPerformingAds, adSender)

    verify(performanceData).readThreeMonthsOldPerformanceData(same(searchEngine), *, same("TEXT"))(*)
    verify(accountData).readAccounts(same(searchEngine))(*)
    verify(topPerformingAds).topPerformingAds(same(performanceDataFrame),same(accountDataFrame),same(100))(*)
    verify(adSender).sendAdsToServiceClient(same(serviceClient),same(topPerformingAdsDataFrame))(*)
  }

}
