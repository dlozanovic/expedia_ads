package com.expedia

import com.holdenkarau.spark.testing.DataFrameSuiteBase
import org.scalatest.{BeforeAndAfterAll, FlatSpec}

class TopPerformingAdsTest extends FlatSpec  with DataFrameSuiteBase {

  lazy implicit val _spark = spark

  val testObject = new TopPerformingAds

  import spark.implicits._
  import testObject._

  "aggregateClicks" should "summarise clicks grouping them by ad_id and ad_group_id" in {
    val rawPerformance = Seq(
      (1, 1, 1, 2),
      (1, 1, 1, 2),
      (1, 2, 1, 5),
      (2, 1, 2, 2),
      (2, 1, 2, 8),
      (3, 2, 5, 42)
    ).toDF("account_id", "ad_group_id", "ad_id", "clicks")

    val expectedDataFrame = Seq(
      (1, 1, 1, 4),
      (1, 2, 1, 5),
      (2, 1, 2, 10),
      (3, 2, 5, 42)
    ).toDF("account_id", "ad_group_id", "ad_id", "clicks")

    val aggregatedPerformance = aggregateClicks(rawPerformance)

    assertDataFrameDataEquals(expectedDataFrame, aggregatedPerformance)

  }


  "calculateTopN" should "take fist N highest ads from each county" in {
    val topN = 100

    val inputDataFrame = createTopNDataFrame(topN * 2, List("GBR", "USA"))
    val expected = createTopNDataFrame(topN, List("GBR", "USA"))

    val actual = calculateTopN(inputDataFrame, topN)
    assertDataFrameDataEquals(expected, actual)

  }

  "topPerformingAds" should "calculate top performing ads per country" in {
    val performanceData = Seq(
      (1, 1, 1, 2),
      (1, 1, 1, 2),
      (1, 2, 1, 5),
      (2, 1, 2, 2),
      (2, 1, 2, 8),
      (3, 2, 5, 42)
    ).toDF("account_id", "ad_group_id", "ad_id", "clicks")

    val accounts = Seq(
      (1, "ACTIVE", "GBR", "ENG"),
      (2, "ACTIVE", "GBR", "ENG"),
      (3, "ACTIVE", "USA", "ENG")
    ).toDF("account_id", "account_status", "country", "language")

    val actual = topPerformingAds(performanceData, accounts, 2)

    val expected = Seq(
      (1, 1, 2, 10, "ACTIVE", "GBR", "ENG"),
      (1, 2, 1, 5, "ACTIVE", "GBR", "ENG"),
      (3, 2, 5, 42, "ACTIVE", "USA", "ENG")
    ).toDF("account_id", "ad_group_id", "ad_id", "clicks", "account_status", "country", "language")

    assertDataFrameDataEquals(expected, actual)

  }

  def createTopNDataFrame(topN: Int, countries: List[String]) = {
    for (i <- (1 to topN);
         country <- countries)
      yield (i, country, 1000 - i)
  }.toDF("id", "country", "clicks")


}
