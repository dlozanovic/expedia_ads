package com.expedia

import java.io.File

import com.holdenkarau.spark.testing.DataFrameSuiteBase
import org.apache.spark.SparkConf
import org.apache.spark.sql.internal.StaticSQLConf.CATALOG_IMPLEMENTATION
import org.scalatest.{BeforeAndAfterAll, FlatSpec}


class AccountDataTest extends FlatSpec with BeforeAndAfterAll with DataFrameSuiteBase{
  override def conf: SparkConf = super.conf.set(CATALOG_IMPLEMENTATION.key, "hive")
  lazy implicit val _spark = spark

  val testObject = new AccountData
  import testObject._

  override def beforeAll(): Unit ={
    super.beforeAll()

    spark.sql(
      """
        |CREATE TABLE accounts (
        |   `account_id` bigint,
        |   `account_status` string   COMMENT 'ACTIVE or INACTIVE',
        |   `country` string          COMMENT 'ISO-3 country code, e.g. USA or GBR',
        |   `language` string         COMMENT 'ISO-3 language code, e.g. ENG or SPA'
        |)
        |PARTITIONED BY (
        |  `search_engine` string     COMMENT 'e.g. GOOGLE or BING',
        |  `report_date` string       COMMENT 'format YYYY-MM-dd e.g. 2020-10-12'
        |)
        |STORED AS PARQUET
        |""".stripMargin)

    spark.sql(
      """INSERT INTO accounts PARTITION (search_engine='GOOGLE', report_date='2020-06-12')
        |VALUES (1,'ACTIVE','USA','ENG')
        |""".stripMargin)

    spark.sql(
      """INSERT INTO accounts PARTITION (search_engine='GOOGLE', report_date='2020-06-12')
        |VALUES (2,'ACTIVE','USA','SPA')
        |""".stripMargin)
    spark.sql(
      """INSERT INTO accounts PARTITION (search_engine='GOOGLE', report_date='2020-06-12')
        |VALUES (3,'INACTIVE','USA','ENG')
        |""".stripMargin)
    spark.sql(
      """INSERT INTO accounts PARTITION (search_engine='BING', report_date='2020-06-12')
        |VALUES (4,'ACTIVE','USA','ENG')
        |""".stripMargin)

    spark.sql(
      """INSERT INTO accounts PARTITION (search_engine='GOOGLE', report_date='2020-06-12')
        |VALUES (5,'ACTIVE','CRO','HRV')
        |""".stripMargin)

    spark.sql(
      """INSERT INTO accounts PARTITION (search_engine='GOOGLE', report_date='2020-06-11')
        |VALUES (1,'ACTIVE','USA','ENG')
        |""".stripMargin)

    spark.sql(
      """INSERT INTO accounts PARTITION (search_engine='GOOGLE', report_date='2020-06-11')
        |VALUES (2,'ACTIVE','USA','SPA')
        |""".stripMargin)
    spark.sql(
      """INSERT INTO accounts PARTITION (search_engine='GOOGLE', report_date='2020-06-11')
        |VALUES (3,'ACTIVE','USA','ENG')
        |""".stripMargin)
    spark.sql(
      """INSERT INTO accounts PARTITION (search_engine='BING', report_date='2020-06-11')
        |VALUES (4,'ACTIVE','USA','ENG')
        |""".stripMargin)

  }

  override def afterAll(): Unit = {
    super.afterAll()
    import scala.reflect.io.Directory

    val metastore = new Directory(new File("metastore_db"))
    metastore.deleteRecursively()

    val warehouse = new Directory(new File("spark-warehouse"))
    warehouse.deleteRecursively()

  }

  "latestPartitionDate" should "for given search_engine give latest report_date" in {
    val date = latestPartitionDate("GOOGLE")

    assert( date == Some("2020-06-12"))
  }

  "readAccounts" should "return all data for given report_date and search_engine" in {

    val google12 = readAllAccounts("GOOGLE","2020-06-12")
    assert(google12.count == 4)

    val google11 = readAllAccounts("GOOGLE","2020-06-11")
    assert(google11.count == 3)

    val bing12 = readAllAccounts("BING","2020-06-12")
    assert(bing12.count == 1)
  }

  "filterAccounts" should "give only active accounts and with language ENG and SPA" in {
    val allAccounts12 = readAllAccounts("GOOGLE","2020-06-12")
    val filteredAccounts12 = filterAccounts(allAccounts12, Set("ENG","SPA"))
    assert( filteredAccounts12.count == 2 )

    val allAccounts11 = readAllAccounts("GOOGLE","2020-06-11")
    val filteredAccounts11 = filterAccounts(allAccounts11, Set("ENG","SPA"))
    assert( filteredAccounts11.count == 3 )

  }

  "readAccounts" should "read all active accounts with language ENG and SPA" in {
    val df = readAccounts("GOOGLE")
    assert(df.count == 2)


    val accountIds = df.select("account_id")
      .collect()
      .map(_.getLong(0)).toSet

    assert(accountIds == Set(1L,2L))
  }

  it should "return empty dataframe with table structure " in {
    val df = readAccounts("YAHOO")

    assert(df.count == 0)
    assert(df.schema.toList.size == 6)
  }

}
