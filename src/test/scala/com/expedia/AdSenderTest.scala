package com.expedia

import com.holdenkarau.spark.testing.DataFrameSuiteBase
import org.mockito.{ArgumentMatchersSugar, MockitoSugar}
import org.scalatest.FlatSpec

import scala.util.Try


class AdSenderTest extends FlatSpec with DataFrameSuiteBase with MockitoSugar with ArgumentMatchersSugar {

  lazy implicit val _spark = spark
  val testObject = new AdSender

  import testObject._
  import spark.implicits._


  "takeElements" should "take no more than maximum specified number of elements" in {
    val rows = generateDataFrame(50).collect().toList

    val batch1 = takeElements(rows.iterator,25)
    assert(batch1.length == 25)

    val batch2 = takeElements(rows.iterator,100)
    assert(batch2.length == 50)
  }

  "sendAds" should "send data in batch to microservice" in {
    val rows = generateDataFrame(49).collect().toList
    val serviceClient = mock[ServiceClient]

    when(serviceClient.sendAdIds(any, any)).thenReturn(Try())

    sendAds(rows.iterator,serviceClient,10)

    verify(serviceClient,times(5)).sendAdIds(any,any)

  }

  "sendAdsToServiceClient" should "send data to the service client" in {
    val rows = generateDataFrame(42,List("ENG","SPA"))

    // could not make mock serializable
    val stub = new ServiceClient with Serializable {
      val timesCalled = spark.sparkContext.longAccumulator("timesCalled")
      val elements = spark.sparkContext.longAccumulator("elements")
      override def sendAdIds(language: String, adIdsBatch: Seq[(AdGroupId, AdId)]): Try[Unit] = {
        timesCalled.add(1)
        elements.add(adIdsBatch.size)
        Try()
      }
    }

    sendAdsToServiceClient(stub,rows)

    assert(stub.timesCalled.value == 4)
    assert(stub.elements.value == 84)
  }


  def generateDataFrame(numberOfElementsPerLanguage: Int,languages:List[String] = List("ENG")) = {
    for (i <- (1 to numberOfElementsPerLanguage);
         language <- languages)
      yield (language, i.toLong, i.toLong)
  }.toDF("language", "ad_group_id","ad_id")

}
