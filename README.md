
#### 1. Select performance data

Table containing daily ad performance reports has the following schema:
```
CREATE TABLE `ad_performance` (
  `account_id` bigint,
  `ad_group_id` bigint     COMMENT 'ad group id is globally unique (also between accounts)',
  `ad_id` bigint           COMMENT 'ad id is only unique within an ad group',
  `ad_type` string         COMMENT 'TEXT or IMAGE',
  `clicks` bigint          COMMENT 'number of clicks an ad received on specific day'
  )
PARTITIONED BY (
  `search_engine` string   COMMENT 'e.g. GOOGLE or BING',
  `report_date` string     COMMENT 'format YYYY-MM-dd, e.g. 2020-10-12'
)
STORED AS PARQUET
```
The table is incremental, meaning that every day data with all clicks recorded on a given day is added to a new table partition.

**Task:** Read last three months performance data of Google text ads.

#### 2. Read account data

The table containing information about all advertising accounts has the following schema:
```
CREATE TABLE accounts (
   `account_id` bigint,
   `account_status` string   COMMENT 'ACTIVE or INACTIVE',
   `country` string          COMMENT 'ISO-3 country code, e.g. USA or GBR',
   `language` string         COMMENT 'ISO-3 language code, e.g. ENG or SPA'
)
PARTITIONED BY (
  `search_engine` string     COMMENT 'e.g. GOOGLE or BING',
  `report_date` string       COMMENT 'format YYYY-MM-dd e.g. 2020-10-12'
)
STORED AS PARQUET
```
That table is NOT incremental. Every day data with all search engine accounts is stored in a new table partition.

**Task:** Read all currently active accounts in Google search engine with ads in Spanish and English. Avoid unneeded 
reads of data from storage.

#### 3. Find top performing ads

**Task:** Find top 100 most clicked Google text ads in English or Spanish in last 3 months per country. Amount of advertising
greatly varies between countries. For example, in USA there's much more ads than in Lichtenstein. Avoid skewed computation. 

#### 4. Send found ads to a microservice

**Task**: Send ad identifiers of selected top most clicked text ads to a microservice in batches of max 25 identifiers. 
The service client implements the following interface.
```
trait ServiceClient {
  type AdGroupId = Long
  type AdId = Long
  def sendAdIds(language: String, adIdsBatch: Seq[(AdGroupId, AdId)]): Try[Unit]
}
```
Utilize cluster evenly, but keep small number of requests.

#### MY Comments

I had to downgrade scalatest to 3.0 branch because spark testing base is not working with 3.1

*** RUN ABORTED ***
  java.lang.NoSuchMethodError: 'org.scalatest.Assertions$AssertionsHelper org.scalatest.Assertions.assertionsHelper()'
  at com.holdenkarau.spark.testing.TestSuite$class.assert(TestSuite.scala:19)
  at com.expedia.TopPerformingAdsTest.assert(TopPerformingAdsTest.scala:9)
  at com.holdenkarau.spark.testing.DataFrameSuiteBaseLike$class.assertDataFrameDataEquals(DataFrameSuiteBase.scala:181)

 